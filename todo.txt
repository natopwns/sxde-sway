To Do
=====

- update install script to handle sway and nwg configs
- handle power button with power off menu (like xfce)
- handle brightness, volume keys
- auto lock / suspend
- default apps
- gui config app for modifying sway

Notes
=====

- added nwg-wrapper for desktop widget calendar
- edited keybindings in config
- edited styling in config

