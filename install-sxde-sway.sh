#!/bin/bash

echo "Programs to install:"
cat programs.txt

# sway config
mkdir ~/.config/sway
cp -ru /etc/sway/config ~/.config/sway/config

# sddm configs
sudo cp -ru Configs/sddm/sddm.conf.d /etc/

# sddm theme
sudo cp -ru Configs/sddm-chili /usr/share/sddm/themes/
